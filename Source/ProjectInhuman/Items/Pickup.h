// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Pickup.generated.h"

UCLASS()
class PROJECTINHUMAN_API APickup : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	APickup();

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Pickup")
		class USphereComponent* CollisionSphere;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Pickup")
		class UDecalComponent* DecalComp;

	UPROPERTY(EditAnywhere, Category = "Pickup")
		TSubclassOf<class APowerupPickup> PowerupClass;

	UPROPERTY(EditDefaultsOnly, Category = "Pickup")
	float CooldownDuration;

	class APowerupPickup* PowerupInstance;
	
	FTimerHandle TimerHandle_Respawn;

protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	void Respawn();

public:
	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
};
