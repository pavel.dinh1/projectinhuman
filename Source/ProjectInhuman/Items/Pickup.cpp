// Fill out your copyright notice in the Description page of Project Settings.


#include "Pickup.h"
#include "Components/SphereComponent.h"
#include "Components/DecalComponent.h"
#include "PowerupPickup.h"
#include "TimerManager.h"

// Sets default values
APickup::APickup()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	CollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionSphere"));
	CollisionSphere->SetGenerateOverlapEvents(true);
	CollisionSphere->SetSphereRadius(75.f);

	DecalComp = CreateDefaultSubobject<UDecalComponent>(TEXT("DecalComp"));
	DecalComp->SetupAttachment(CollisionSphere);
	DecalComp->DecalSize = FVector(37.5f, 75.f, 75.f);
	SetReplicates(true);

}

void APickup::BeginPlay()
{
	Super::BeginPlay();
	Respawn();
}

void APickup::Respawn()
{
	if (!PowerupClass)
	{
		UE_LOG(LogTemp, Error, TEXT("PowerupClass is nullptr in %s. Please update your blueprint !"), *GetName());
		return;
	}

	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	PowerupInstance = GetWorld()->SpawnActor<APowerupPickup>(PowerupClass, GetTransform(), SpawnParams);
	if (!PowerupInstance)
	{
		UE_LOG(LogTemp, Warning, TEXT("PowerupInstance is nullptr in %s. Please update your blueprint !"), *GetName());
	}
}

void APickup::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	if (PowerupInstance)
	{
		PowerupInstance->ActivatePowerup();
		PowerupInstance = nullptr;
		UE_LOG(LogTemp, Warning, TEXT("PowerupInstance Activated !"));
		GetWorldTimerManager().SetTimer(TimerHandle_Respawn, this, &APickup::Respawn, CooldownDuration);
	}
}
