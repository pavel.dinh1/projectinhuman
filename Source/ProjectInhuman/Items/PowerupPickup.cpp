// Fill out your copyright notice in the Description page of Project Settings.


#include "PowerupPickup.h"
#include "TimerManager.h"

// Sets default values
APowerupPickup::APowerupPickup()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	PowerupInterval = 0.f;
	TotalNumOfTicks = 0.f;
}

// Called when the game starts or when spawned
void APowerupPickup::BeginPlay()
{
	Super::BeginPlay();
}

void APowerupPickup::ActivatePowerup()
{
	OnActivated();
	if (PowerupInterval > 0.f)
	{
		GetWorldTimerManager().SetTimer(TimerHandle_PowerupTick, this, &APowerupPickup::OnTickPowerup, PowerupInterval, true, 0.f);
	}
	else
	{
		OnTickPowerup();
	}
}

void APowerupPickup::OnTickPowerup()
{
	TicksProcessed++;
	
	OnPowerupTicked();

	if (TicksProcessed >= TotalNumOfTicks)
	{
		OnExpired();
		GetWorldTimerManager().ClearTimer(TimerHandle_PowerupTick);
	}
}