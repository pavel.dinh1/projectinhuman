// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PowerupPickup.generated.h"

UCLASS()
class PROJECTINHUMAN_API APowerupPickup : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	APowerupPickup();

protected:
	/* Time between poweup ticks */
	UPROPERTY(EditDefaultsOnly, Category = "Powerup")
		float PowerupInterval;

	/* Total times we apply the powerup effect */
	UPROPERTY(EditDefaultsOnly, Category = "Powerup")
		int32 TotalNumOfTicks;

	FTimerHandle TimerHandle_PowerupTick;

	/* Number of ticks applied */
	int32 TicksProcessed;

	UFUNCTION()
		void OnTickPowerup();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	UFUNCTION()
		void ActivatePowerup();

	UFUNCTION(BlueprintImplementableEvent, Category = "Powerup")
		void OnActivated();
	
	UFUNCTION(BlueprintImplementableEvent, Category = "Powerup")
		void OnPowerupTicked();
	
	UFUNCTION(BlueprintImplementableEvent, Category = "Powerup")
		void OnExpired();
};
