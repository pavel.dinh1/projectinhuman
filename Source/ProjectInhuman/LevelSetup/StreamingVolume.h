// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "StreamingVolume.generated.h"

UCLASS()
class PROJECTINHUMAN_API AStreamingVolume : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AStreamingVolume();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	

public:	

};
