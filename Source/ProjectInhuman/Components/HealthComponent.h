// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "../Interfaces/CharacterEvents.h"
#include "HealthComponent.generated.h"

// OnChangeHealth event
DECLARE_DYNAMIC_MULTICAST_DELEGATE_SixParams (FOnHealthChangedSignature, UHealthComponent*, HealthComp, float, Health, float, HealthDelta, const class UDamageType*, DamageType, class AController*, InstigatedBy, AActor*, DamageCauser);

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class PROJECTINHUMAN_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

protected:

	virtual void BeginPlay() override;

public:

	// Sets default values for this component's properties
	UHealthComponent();

	UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadWrite, Category = "Health Component")
		float CurrentHealth;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Health Component")
		float MaxHealth;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Health Component")
		float PrevHealth;

public:

	UFUNCTION(BlueprintPure, BlueprintCallable, Category = "Health Component")
		float GetCurrentHealth() { return CurrentHealth; }

	UFUNCTION(BlueprintPure, BlueprintCallable, Category = "Health Component")
		float GetMaxHealth() { return MaxHealth; }

	UFUNCTION(BlueprintPure, BlueprintCallable, Category = "Health Component")
		float GetHealthPercent() { return CurrentHealth / MaxHealth; }

	UFUNCTION(BlueprintCallable, Category = "Events")
		void HandleTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

public:
	UPROPERTY(BlueprintAssignable,BlueprintCallable, Category = "Events")
		FOnHealthChangedSignature OnHealthChanged;

	UFUNCTION(BlueprintCallable, Category = "Health Component")
	void Heal(float HealAmount);

};
