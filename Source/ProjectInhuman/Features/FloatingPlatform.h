// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FloatingPlatform.generated.h"

UCLASS()
class PROJECTINHUMAN_API AFloatingPlatform : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AFloatingPlatform();

protected:
	UPROPERTY(EditAnywhere, Category = "FloatingPlatform")
		float Speed;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "MovingPlatform")
		class UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditAnywhere, meta = (MakeEditWidget = true))
		FVector TargetLocation;

private:
	FVector GlobalTargetLocation;
	FVector GlobalStartLocation;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
