// Fill out your copyright notice in the Description page of Project Settings.

#include "Cannon.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Components/SceneComponent.h"
#include "TimerManager.h"
#include "Engine/World.h"
#include "Explosives/ExplodingBall.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundBase.h"
#include "GameFramework/Character.h"
#include "Net/UnrealNetwork.h"

// Sets default values
ACannon::ACannon()
{
	CannonMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CannonMesh"));
	RootComponent = CannonMesh;

	Muzzle = CreateDefaultSubobject<USceneComponent>(TEXT("Muzzle"));
	Muzzle->SetupAttachment(CannonMesh);

	CannonFireTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("CannonFireTrigger"));
	CannonFireTrigger->SetupAttachment(CannonMesh);
	CannonFireTrigger->SetVisibility(false);
}

// Called when the game starts or when spawned
void ACannon::BeginPlay()
{
	Super::BeginPlay();

	if (!bIsTriggerFire)
	{
		CannonFireTrigger->SetVisibility(false);
		GetWorldTimerManager().SetTimer(TimerHandle_ShootingFireBalls, this, &ACannon::ShootCannonBall, FMath::RandRange(1.f, 2.5f), true);
		if (Debug)
		{
			UE_LOG(LogTemp, Warning, TEXT("Cannon Fire Mode : Looping"));
		}
	}
	else
	{
		CannonFireTrigger->SetVisibility(true);
		CannonFireTrigger->OnComponentBeginOverlap.AddDynamic(this, &ACannon::ShootCannonBallOnTrigger);
		if (Debug)
		{
			UE_LOG(LogTemp, Warning, TEXT("Cannon Fire Mode : Trigger"));
		}
	}
}

void ACannon::ShootCannonBall()
{
	FVector ShootDirection = Muzzle->GetComponentLocation() + Muzzle->GetSocketRotation("None").Vector().ForwardVector;
	FQuat BallRotation = Muzzle->GetSocketRotation("None").Quaternion();

	FTransform SpawnTransform;
	SpawnTransform.SetLocation(ShootDirection);
	SpawnTransform.SetRotation(BallRotation);

	FActorSpawnParameters SpawnParams;
	SpawnParams.Owner = this;
	if (GetLocalRole() == ROLE_Authority)
	{
		// Spawn exploding ball
		AExplodingBall* TempExploBall = GetWorld()->SpawnActor<AExplodingBall>(ExplodingBallClass, SpawnTransform, SpawnParams);
		// Play sound if available
		if (CannonFireSound)
		{
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), CannonFireSound, GetActorLocation());
		}

		if (Debug)
		{
			UE_LOG(LogTemp, Warning, TEXT("Firing Explosion Ball. Played CannonFireSound: %s. Instantiated: %s"), *CannonFireSound->GetName(), *TempExploBall->GetName());
		}
	}
}

void ACannon::ShootCannonBallOnTrigger(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (Cast<ACharacter>(OtherActor))
	{
		ShootCannonBall();
	}
}
