// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Cannon.generated.h"

UCLASS()
class PROJECTINHUMAN_API ACannon : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ACannon();

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Cannon")
		class USkeletalMeshComponent* CannonMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Cannon")
		class USceneComponent* Muzzle;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Cannon")
		TSubclassOf<class AExplodingBall> ExplodingBallClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "TriggerFire")
		bool bIsTriggerFire;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "TriggerFire")
		class UBoxComponent* CannonFireTrigger;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Sound")
		class USoundBase* CannonFireSound;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Debug")
		bool Debug;

	FTimerHandle TimerHandle_ShootingFireBalls;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, Category = "CannonFire")
		void ShootCannonBall();

	UFUNCTION(BlueprintCallable, Category = "CannonFire")
		void ShootCannonBallOnTrigger(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
};