// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ExplodingRadius.generated.h"

UCLASS()
class PROJECTINHUMAN_API AExplodingRadius : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AExplodingRadius();

protected:
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly, Category = "Explosion")
		class URadialForceComponent* ExplosionImpact;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Explosion")
		class USphereComponent* ImpactRadius;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Explosion")
		class USceneComponent* Root;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Explosion")
		class UParticleSystem* ExplosionEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Explosion")
		class USoundBase* ExplosionSound;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Explosion")
		float ExplosionDamage;

	FTimerHandle TimerHandle_DestroyDelay;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, Category = "Explosion")
		void ExplodeImpact();

	UFUNCTION()
		void DestroyActor();

	UFUNCTION(BlueprintNativeEvent, Category = "Explosion")
		void DamageCharacterOnImpact(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	void DamageCharacterOnImpact_Implementation(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
};
