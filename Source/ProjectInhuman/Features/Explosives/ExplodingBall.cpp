// Fill out your copyright notice in the Description page of Project Settings.

#include "ExplodingBall.h"
#include "Components/StaticMeshComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "TimerManager.h"
#include "ExplodingRadius.h"
#include "Net/UnrealNetwork.h"

// Sets default values
AExplodingBall::AExplodingBall()
{
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	RootComponent = StaticMesh;

	Flame = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("FlameEffect"));
	Flame->SetupAttachment(StaticMesh);

	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement"));
	ProjectileMovement->InitialSpeed = 2000.f;
	ProjectileMovement->bShouldBounce = true;
	ProjectileMovement->Velocity = FVector(200.f, 0.f, 0.f);

}

// Called when the game starts or when spawned
void AExplodingBall::BeginPlay()
{
	Super::BeginPlay();
	StaticMesh->SetNotifyRigidBodyCollision(true);
	GetWorldTimerManager().SetTimer(TimerHandle_Delay, this, &AExplodingBall::Explode, 1.2f, false);
}

void AExplodingBall::Explode()
{
	if (GetLocalRole() == ROLE_Authority)
	{
		FActorSpawnParameters SpawnParams;
		GetWorld()->SpawnActor<AExplodingRadius>(ExplosionClass, GetActorLocation(), GetActorRotation(), SpawnParams);
	}
	Destroy();
}

