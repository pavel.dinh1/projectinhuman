// Fill out your copyright notice in the Description page of Project Settings.

#include "ExplodingRadius.h"
#include "TimerManager.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "../../Character/PlayerCharacter.h"
#include "Components/SphereComponent.h"
#include "Components/SceneComponent.h"
#include "PhysicsEngine/RadialForceComponent.h"
#include "Net/UnrealNetwork.h"

// Sets default values
AExplodingRadius::AExplodingRadius()
{
	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;

	ImpactRadius = CreateDefaultSubobject<USphereComponent>(TEXT("ImpactRadius"));
	ImpactRadius->SetupAttachment(Root);
	ImpactRadius->SetNotifyRigidBodyCollision(true);

	ExplosionImpact = CreateDefaultSubobject<URadialForceComponent>(TEXT("ExplosionImpact"));
	ExplosionImpact->SetupAttachment(Root);
	ExplosionImpact->Falloff = ERadialImpulseFalloff::RIF_Linear;
	ExplosionImpact->Radius = 200.f;
	ExplosionImpact->ImpulseStrength = 5000.f;
	ExplosionImpact->ForceStrength = 100.f;

	ExplosionDamage = 15.f;
	SetReplicates(true);

}

// Called when the game starts or when spawned
void AExplodingRadius::BeginPlay()
{
	Super::BeginPlay();

	ImpactRadius->OnComponentBeginOverlap.AddDynamic(this, &AExplodingRadius::DamageCharacterOnImpact);
	ImpactRadius->SetSphereRadius(ExplosionImpact->Radius);
	ExplodeImpact();

	GetWorldTimerManager().SetTimer(TimerHandle_DestroyDelay, this, &AExplodingRadius::DestroyActor, 0.1f);
}

void AExplodingRadius::ExplodeImpact()
{
	if (GetLocalRole() == ROLE_Authority)
	{
		ExplosionImpact->FireImpulse();

		if (ExplosionEffect)
		{
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionEffect, GetActorLocation());
		}

		if (ExplosionSound)
		{
			UGameplayStatics::SpawnSoundAtLocation(GetWorld(), ExplosionSound, GetActorLocation());
		}
	}
}

void AExplodingRadius::DestroyActor()
{
	Destroy();
}

void AExplodingRadius::DamageCharacterOnImpact_Implementation(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (Cast<APlayerCharacter>(OtherActor))
		UE_LOG(LogTemp, Warning, TEXT("FOUND A PLAYER AND DAMAGING IT !"));
}
