// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ExplodingBall.generated.h"

UCLASS()
class PROJECTINHUMAN_API AExplodingBall : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AExplodingBall();

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "ExplodingBall")
		class UStaticMeshComponent* StaticMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "ExplodingBall")
		class UParticleSystemComponent* Flame;

	UPROPERTY(VisibleAnywhere, Category = "ExplodingBall")
		class UProjectileMovementComponent* ProjectileMovement;
	
	UPROPERTY(EditDefaultsOnly, Category = "ExplodingBall")
		TSubclassOf<class AExplodingRadius> ExplosionClass;

	FTimerHandle TimerHandle_Delay;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, Category = "Explosion")
		void Explode();
};
