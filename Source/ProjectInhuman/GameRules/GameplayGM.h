// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GameplayGM.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FPlayerDied);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FCharacterDied);

/**
 * 
 */
UCLASS()
class PROJECTINHUMAN_API AGameplayGM : public AGameModeBase
{
	GENERATED_BODY()

		AGameplayGM();

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Spawning")
		TArray<class ASpawnPoint*> SpawnPoints;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawning")
		int32 SpawnIndex;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Spawning")
		TArray<class APlayerStart*> PlayerStarts;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Spawning")
		bool bDebugPlayerStartsArray;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Player")
		TSubclassOf<class ACharacter> PlayerClass;

protected:
	UFUNCTION(BlueprintCallable, Category = "Spawning")
		void SetupSpawnPoints();

	UFUNCTION(BlueprintCallable, Category = "Spawning")
		TArray<class APlayerStart*> GetPlayerStarts();

	UFUNCTION()
		TArray<class ASpawnPoint*> BubbleSortSpawnPoints(TArray<class ASpawnPoint*> SpawnArray);

public:
	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "EventDispatchers")
		FPlayerDied PlayerDied;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "EventDispatchers")
		FCharacterDied CharacterDied;
public:
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, Category = "Spawning")
		void SpawnPlayerAtSpawnPoint(int32 index, bool bIsAI);

	UFUNCTION(BlueprintCallable, Category = "Spawning")
		class APlayerCharacter* RandomPlayerStart(TArray<class APlayerStart*> PlayerStartsArray, bool bIsAI);

	UFUNCTION(BlueprintImplementableEvent, Blueprintcallable, Category = "Event")
		void OnPlayerDied();

	UFUNCTION(BlueprintImplementableEvent, Blueprintcallable, Category = "Event")
		void OnCharacterDied();

	UFUNCTION(BlueprintCallable, Category = "SpawnPoints")
		void SetSpawnIndex(int index);
};