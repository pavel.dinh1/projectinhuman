// Fill out your copyright notice in the Description page of Project Settings.


#include "GameplayGM.h"
#include "../LevelSetup/SpawnPoint.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "../Character/PlayerCharacter.h"
#include "GameFramework/PlayerStart.h"
#include "../Components/HealthComponent.h"

AGameplayGM::AGameplayGM()
{
	SpawnIndex = 0;
	PlayerClass = nullptr;
}

void AGameplayGM::BeginPlay()
{
	Super::BeginPlay();
	PlayerDied.AddDynamic(this, &AGameplayGM::OnPlayerDied);
	CharacterDied.AddDynamic(this, &AGameplayGM::OnCharacterDied);
}

void AGameplayGM::SetupSpawnPoints()
{
	TArray<AActor*> TempArr;
	UGameplayStatics::GetAllActorsOfClass(this, ASpawnPoint::StaticClass(), TempArr);
	for (AActor* var : TempArr)
	{
		if (ASpawnPoint* SpawnPoint = Cast<ASpawnPoint>(var))
		{
			SpawnPoints.Add(SpawnPoint);
		}
	}

	SpawnPoints = BubbleSortSpawnPoints(SpawnPoints);

	if (bDebugPlayerStartsArray)
	{
		for (ASpawnPoint* sp : SpawnPoints)
		{
			UE_LOG(LogTemp, Warning, TEXT("Spawning Point: %s %d"), *sp->GetName(), sp->SpawnPointIndex);
		}
	}
}

TArray<class APlayerStart*> AGameplayGM::GetPlayerStarts()
{
	TArray<AActor*> TempArr;
	UGameplayStatics::GetAllActorsOfClass(this, APlayerStart::StaticClass(), TempArr);
	for (AActor* var : TempArr)
	{
		if (APlayerStart* PlayerStart = Cast<APlayerStart>(var)) {
			PlayerStarts.Add(PlayerStart);
			UE_LOG(LogTemp, Warning, TEXT("PlayerStarts: %s"), *var->GetName());
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("PlayerStarts: %s is INVALID !"), *var->GetName());
		}
	}
	if (bDebugPlayerStartsArray)
	{
		if (PlayerStarts.Num() > 0)
		{
			for (AActor* var : PlayerStarts)
			{
				UE_LOG(LogTemp, Warning, TEXT("PlayerStarts: %s"), *var->GetName());
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("The array is not filled with PlayerStart Actors ! -> Reference to function GetPlayerStarts() -> PKGameMode.cpp"));
		}
	}
	return PlayerStarts;
}

TArray<class ASpawnPoint*> AGameplayGM::BubbleSortSpawnPoints(TArray<class ASpawnPoint*> SpawnArray)
{
	int32 n = SpawnPoints.Num();
	ASpawnPoint* temp = nullptr;
	for (int32 i = 0; i < n; i++)
	{
		for (int32 x = 1; x < (n - i); x++)
		{
			if (SpawnArray[x - 1]->SpawnPointIndex > SpawnArray[x]->SpawnPointIndex)
			{
				temp = SpawnArray[x - 1];
				SpawnArray[x - 1] = SpawnArray[x];
				SpawnArray[x] = temp;
			}
		}
	}
	return SpawnArray;
}

void AGameplayGM::SetSpawnIndex(int index)
{
	SpawnIndex = index;
}

void AGameplayGM::SpawnPlayerAtSpawnPoint(int32 index, bool bIsAI)
{
	SpawnIndex = index;
	if (SpawnPoints.IsValidIndex(index))
	{
		if (PlayerClass)
		{
			ACharacter* Player = GetWorld()->SpawnActor<ACharacter>(PlayerClass, SpawnPoints[index]->GetActorLocation(), SpawnPoints[index]->GetActorRotation());
			if (!bIsAI && Player)
			{
				APlayerController* PC = UGameplayStatics::GetPlayerController(GetWorld(), 0);
				PC->Possess(Player);
			}
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("Player class hasn't been initialized in the GAME MODE !"));
		}
	}
}

APlayerCharacter* AGameplayGM::RandomPlayerStart(TArray<class APlayerStart*> PlayerStartsArray, bool bIsAI)
{
	// Generate random number between number of the player starts in the level
	int32 randomPlayerStart = FMath::RandRange(0, PlayerStartsArray.Num() - 1);
	APlayerCharacter* Player = nullptr;
	if (PlayerStartsArray.IsValidIndex(randomPlayerStart) && PlayerStartsArray.Num() > 0)
	{
		Player = GetWorld()->SpawnActor<APlayerCharacter>(PlayerClass, PlayerStartsArray[randomPlayerStart]->GetActorLocation(), PlayerStartsArray[randomPlayerStart]->GetActorRotation());

		if (!Player)
		{
			UE_LOG(LogTemp, Error, TEXT("Player class hasn't been initialized in the GAME MODE !"));
			return nullptr;
		}
		if (!bIsAI)
		{
			APlayerController* PC = UGameplayStatics::GetPlayerController(GetWorld(), 0);
			PC->Possess(Player);
		}
		Player->OnSpawn();
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Player class hasn't been initialized in the GAME MODE !"));
	}
	return Player;
}

