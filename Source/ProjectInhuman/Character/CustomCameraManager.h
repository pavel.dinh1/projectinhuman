// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/PlayerCameraManager.h"
#include "CustomCameraManager.generated.h"

/**
 *
 */
UCLASS()
class PROJECTINHUMAN_API ACustomCameraManager : public APlayerCameraManager
{
	GENERATED_BODY()

public:
	ACustomCameraManager();

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera")
		class USkeletalMeshComponent* CameraBehavior;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera")
		class USkeletalMeshComponent* SteadyCamera;

public:
	FVector GetCameraLocation() const;

	UFUNCTION()
		USkeletalMeshComponent* GetSteadyCamera() { return SteadyCamera; };
};
