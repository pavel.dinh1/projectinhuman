// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "../Interfaces/CharacterEvents.h"
#include "PlayerCharacter.generated.h"

UCLASS()
class PROJECTINHUMAN_API APlayerCharacter : public ACharacter, public ICharacterEvents
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		class UHealthComponent* HealthComp;

protected:

public:
	// Sets default values for this character's properties
	APlayerCharacter();

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	class UHealthComponent* GetHealthComp()
	{
		return HealthComp;
	}

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
		void OnDead(bool IsDead);
	void OnDead_Implementation(bool IsDead);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
		void OnSpawn();
	void OnSpawn_Implementation();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Events")
		void OnPlayerHealthChanged(class UHealthComponent* HealthCompRef, float CurrHealth, float HealthDeltaRef, const class UDamageType* DamageTypeRef, class AController* InstigatedByRef, AActor* DamageCauserRef);
};
