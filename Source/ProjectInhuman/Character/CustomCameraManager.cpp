// Fill out your copyright notice in the Description page of Project Settings.


#include "CustomCameraManager.h"
#include "Components/SkeletalMeshComponent.h"

ACustomCameraManager::ACustomCameraManager()
{
	CameraBehavior = CreateDefaultSubobject< USkeletalMeshComponent>(TEXT("CameraBehavior"));
	CameraBehavior->SetupAttachment(GetRootComponent());
	SteadyCamera= CreateDefaultSubobject< USkeletalMeshComponent>(TEXT("SteadyCamera"));
	SteadyCamera->SetupAttachment(GetRootComponent());

}

FVector ACustomCameraManager::GetCameraLocation() const
{
	return CameraBehavior->GetComponentLocation();
}